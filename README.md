# shurjoPay Android SDK #

We can easily implement payment gateway of shurjoPay through this SDK.
Here, we have the ability to set and define merchant information and required parameters. Please follow the below instructions.

* shujoPay andorid SDK.
* 1.1.0
* Documentation can be download form the link below:

# Dependencies #
Add the following dependencies:

implementation "com.squareup.retrofit2:retrofit:2.6.0"
implementation "com.squareup.retrofit2:converter-gson:2.6.0"
implementation 'com.squareup.retrofit2:converter-scalars:2.6.0'
implementation 'com.google.code.gson:gson:2.8.5'
implementation 'com.squareup.okhttp3:okhttp:4.2.2'
implementation 'com.squareup.okhttp3:logging-interceptor:4.2.2'

# Proguard #
If you use proguard, add the following lines in proguard-rules.pro:

-keep class com.sm.shurjopaysdk.model.TransactionInfo**
-keepclassmembers class com.sm.shurjopaysdk.model.TransactionInfo {*;}

# Adding shurjoPay SDK Library #
Please include shurjoPay Android SDK on your existing project as per our described instructions.
SDK Name: shurjopaysdk-v1.1.aar

For more details please head into the link below:

https://docs.google.com/document/d/1WWrVc4yoxGkl-9Z3Bv_QIMdaM4TpnJx1fouhdfqi8Fo/edit?usp=sharing

